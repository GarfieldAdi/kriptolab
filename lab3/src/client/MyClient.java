package client;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public class MyClient {

	String host = "bnr.ro";
	Integer port = 443;
	String fileName = "HTMLFile.txt";

	private void getConnection() throws IOException {
		System.setProperty("javax.net.ssl.keyStore", "client.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
		System.setProperty("javax.net.ssl.keyStoreType", "jks");
		System.setProperty("javax.net.ssl.trustStore", "server.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
		System.setProperty("javax.net.ssl.trustStoreType", "jks");

		SSLSocket sslSocket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(host, port);
		sslSocket.startHandshake();

		PrintWriter getRequest = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sslSocket.getOutputStream())));
		getRequest.println("GET /Home.aspx HTTP/1.0");
		getRequest.println();
		getRequest.flush();

		BufferedReader reader = new BufferedReader(new InputStreamReader(sslSocket.getInputStream()));
		String line;
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		while ((line = reader.readLine()) != null) {
			writer.write(line);
			writer.write("\n");
		}
		writer.close();

		Certificate[] certificates = sslSocket.getSession().getPeerCertificates();
		X509Certificate x509Certificate = (X509Certificate) certificates[0];
		System.out.println("Version: " + x509Certificate.getVersion());
		System.out.println("Serial number: " + x509Certificate.getSerialNumber());
		System.out.println("Signature algorithm: " + x509Certificate.getSigAlgName());
		System.out.println("Issuer: " + x509Certificate.getIssuerDN());
		System.out.println("Valid from: " + x509Certificate.getNotBefore());
		System.out.println("Valid to: " + x509Certificate.getNotAfter());
		System.out.println("Subject: " + x509Certificate.getSubjectDN());
		System.out.println("Public key: " + x509Certificate.getPublicKey());
	}

	public static void main(String[] args) throws IOException {
		MyClient bnrClient = new MyClient();
		bnrClient.getConnection();
	}

}
