package server;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class BnrServer {

    public static void main(String[] args) throws IOException {
        System.setProperty("javax.net.ssl.keyStore", "bnr_copy.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
        System.setProperty("javax.net.ssl.keyStoreType", "jks");
        System.setProperty("javax.net.ssl.trustStore", "bnr_copy.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
        System.setProperty("javax.net.ssl.trustStoreType", "jks");

        try (SSLServerSocket sslServerSocket = (SSLServerSocket) SSLServerSocketFactory.getDefault().createServerSocket(443)) {
            sslServerSocket.setNeedClientAuth(true);
            sslServerSocket.setEnabledProtocols(new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"});

            try (Socket socket = sslServerSocket.accept()) {
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                Scanner scanner = new Scanner(new File("HTMLFile.txt"));
                while (scanner.hasNextLine()) {
                    String html = scanner.nextLine();
                    out.println(html);
                }
                scanner.close();
            } catch (Exception e) {
                System.out.println("Error!!");
            }
        }

    }
}
