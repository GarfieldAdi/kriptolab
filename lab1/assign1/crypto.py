#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this with a description of the program.
"""
import cryptoUtils
import math
import random
import fractions
import exceptions

# Caesar Cipher

def encrypt_caesar(plaintext):
    [isCorrectInput, message] = exceptions.iscorrect_input_caesar(plaintext)
    if (not isCorrectInput):
        raise(Exception(message))

    ciphertext = ""
    for char in plaintext:
        if (char.isalpha()):
            cipherChar = chr((ord(char) - ord('A') + 3) % 26 + ord('A'))
            ciphertext = ciphertext + cipherChar
        else:
            ciphertext = ciphertext + char
    print(ciphertext)


def decrypt_caesar(ciphertext):
    [isCorrectInput, message] = exceptions.iscorrect_input_caesar(ciphertext)
    if (not isCorrectInput):
        raise(Exception(message))

    plaintext = ""
    for char in ciphertext:
        if (char.isalpha()):
            plainChar = chr((ord(char) - ord('A') - 3) % 26 + ord('A'))
            plaintext = plaintext + plainChar
        else:
            plaintext = plaintext + char
    print(plaintext)

def encrypt_caesar_binary(plainbinary):
    cipherBinary = bytearray()
    for binary in plainbinary:
        cipherBinary.extend(((binary + 3) % 256).to_bytes(1, byteorder='big'))
    byteCipher = bytes(cipherBinary)
    print(byteCipher.decode('utf-8'))


def decrypt_caesar_binary(cipherbinary):
    plainBinary = bytearray()
    for binary in cipherbinary:
        plainBinary.extend(((binary - 3) % 256).to_bytes(1, byteorder='big'))
    bytePlain = bytes(plainBinary)
    print(bytePlain)


# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    [isCorrectInput, message] = exceptions.iscorrect_input_vigenere(plaintext, keyword)
    if (not isCorrectInput):
        raise(Exception(message))
    
    ciphertext = ""
    keyLen = len(keyword)
    for ind,char in enumerate(plaintext):
        if (char.isalpha()):
            cipherChar = chr((ord(char) + ord(keyword[ind%keyLen]) - 2*ord('A')) % 26 + ord('A'))
            ciphertext = ciphertext + cipherChar
        else:
            ciphertext = ciphertext + char
    print(ciphertext)


def decrypt_vigenere(ciphertext, keyword):
    [isCorrectInput, message] = exceptions.iscorrect_input_vigenere(ciphertext, keyword)
    if (not isCorrectInput):
        raise(Exception(message))
    plaintext = ""
    keyLen = len(keyword)
    for ind,char in enumerate(ciphertext):
        if (char.isalpha()):
            plainChar = chr((ord(char) - ord(keyword[ind%keyLen])) % 26 + ord('A'))
            plaintext = plaintext + plainChar
        else:
            plaintext = plaintext + char
    return plaintext

def decrypt_vigenere_without_keyword(ciphertext):
    dict = cryptoUtils.readDictionary()
    ciphertextWords = cryptoUtils.getWordsFromText(ciphertext)
    maxGoodWords = 0
    bestDecryptedText = ""
    textLength = len(ciphertext)

    for keyword in dict:
        if (len(keyword) > 1):
            fullDecryptedText = decrypt_vigenere(ciphertext, keyword)
            goodWords = len([word for word in fullDecryptedText.split() if word in dict])
            if (goodWords > maxGoodWords):
                maxGoodWords = goodWords
                bestDecryptedText = fullDecryptedText
                if (maxGoodWords == textLength):
                    break
    print(bestDecryptedText)


#Scytale Cipher
def encrypt_scytale(plaintext, circumference):
    [isCorrectInput, message] = exceptions.iscorrect_input_scytale(plaintext, circumference)
    if (not isCorrectInput):
        raise(Exception(message))
    
    circumference = int(circumference)
    cipherArray = []
    for circInt in range(circumference):
        cipherArray.append(plaintext[circInt::circumference])
    ciphertext = ""
    for cipherElement in cipherArray:
        ciphertext += cipherElement
    print(ciphertext)


def decrypt_scytale(ciphertext, circumference):
    [isCorrectInput, message] = exceptions.iscorrect_input_scytale(ciphertext, circumference)
    if (not isCorrectInput):
        raise(Exception(message))

    circumference = int(circumference)
    cipherLen = len(ciphertext)
    if (cipherLen % circumference == 0):
        completeLines = circumference
    else:
        completeLines = cipherLen % circumference
    lettersInLine = math.ceil(cipherLen / circumference)
    truncLines = circumference - completeLines

    plainArray = []
    for lineInd in range(completeLines):
        plainArray.append(ciphertext[lineInd*lettersInLine:(lineInd*lettersInLine+lettersInLine)])
    usedLetters = completeLines * lettersInLine
    for lineInd in range(truncLines):
        startInd = usedLetters + lineInd*(lettersInLine-1)
        endInd = startInd + lettersInLine - 1
        plainArray.append(ciphertext[startInd:endInd])

    plaintext = ""
    for letterInd in range(lettersInLine-1):
        plaintext += ''.join(letters[letterInd] for letters in plainArray)
    plaintext += ''.join(letters[lettersInLine-1] for letters in plainArray[0:completeLines])

    print(plaintext)

#Railfence Cipher
def encrypt_railfence(plaintext, num_rails):
    [isCorrectInput, message] = exceptions.iscorrect_input_railfence(plaintext, num_rails)
    if (not isCorrectInput):
        raise(Exception(message))

    num_rails = int(num_rails)
    tempArray = []
    for i in range(num_rails):
        tempArray.append("")
    railInd = 0
    desc = True
    for letter in plaintext:
        tempArray[railInd] += letter
        if (desc):
            if (railInd == num_rails -2):
                desc = False
            railInd = railInd + 1 
        else:
            if (railInd == 1):
                desc = True
            railInd = railInd - 1
    
    ciphertext = ''.join(tempArray)
    print(ciphertext)



def decrypt_railfence(ciphertext, num_rails):
    [isCorrectInput, message] = exceptions.iscorrect_input_railfence(ciphertext, num_rails)
    if (not isCorrectInput):
        raise(Exception(message))

    num_rails = int(num_rails)
    tempArray = []
    numLetters = []
    for i in range(num_rails):
        tempArray.append("")
        numLetters.append(0)
    railInd = 0
    desc = True        

    for letter in range(len(ciphertext)):
        numLetters[railInd] += 1
        if (desc):
            if (railInd == num_rails -2):
                desc = False
            railInd = railInd + 1 
        else:
            if (railInd == 1):
                desc = True
            railInd = railInd - 1
    
    lastInd = 0
    for (ind, numLetter) in enumerate(numLetters):
        tempArray[ind] = ciphertext[lastInd:(lastInd+numLetter)]
        lastInd += numLetter
    
    plaintext = ""

    railInd = 0
    desc = True
    lastIndexes = []
    for i in range(num_rails):
        lastIndexes.append(0)

    for letter in range(len(ciphertext)):
        plaintext += tempArray[railInd][lastIndexes[railInd]]
        lastIndexes[railInd] += 1
        if (desc):
            if (railInd == num_rails - 2):
                desc = False
            railInd = railInd + 1 
        else:
            if (railInd == 1):
                desc = True
            railInd = railInd - 1

    print(plaintext)


# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    w = [random.randint(2, 10)]
    sum = w[0]
    for i in range(1,8):
        w.append(random.randint(sum+1, 2*sum))
        sum += w[i]
    q = random.randint(sum+1, 2*sum)
    isCoprime = False
    while not isCoprime:
        r = random.randint(2, q-1)
        print(cryptoUtils.coprime(q, r))
        isCoprime = (1 == cryptoUtils.coprime(q, r))
    return (w, q, r)

def create_public_key(private_key):
    (w, q, r) = private_key
    beta = []
    for i in range (8):
        beta.append((r * w[i]) % q)
    return beta


def encrypt_mh(message, public_key):
    newChar = []
    for char in message:
        bitsArray = cryptoUtils.byte_to_bits(ord(char))
        newByte = sum(bit*key for bit, key in zip(bitsArray, public_key))
        newChar.append(newByte)
    return newChar

def decrypt_mh(message, private_key):
    (w, q, r) = private_key
    s = cryptoUtils.modinv(r, q)
    dc = []
    for c in message:
        dc.append((c * s) % q)
    dMessage = []
    for bit in dc:
        bits = [1] * 8
        for i in range(7, -1, -1):
            if w[i] > bit:
                bits[i] = 0
            bit = bit - w[i] * bits[i]
        dMessage.append(chr(cryptoUtils.bits_to_byte(bits)))
    return dMessage

