def iscorrect_input_caesar(text):
    if len(text) == 0:
        return [False, "The length is zero!"]
    if not text.isupper():
        return [False, "Alphabetic characters are not uppercase!"]
    return [True, "Correct input"]

def iscorrect_input_vigenere(text, keyword):
    if len(text) == 0 or len(keyword) < 2:
        return [False, "The length is not sufficient!"]
    if not keyword.isalpha():
        return [False, "The words contain not alphabetic characters!"]
    if all(not letter.isalpha() and not letter.isspace() for letter in text):
        return [False, "The words contain not alphabetic characters!"]
    if not text.isupper():
        return [False, "Characters are not uppercase!"]
    return [True, "Correct input"]

def iscorrect_input_scytale(text, circumference):
    if len(text) == 0:
        return [False, "The length is zero!"]
    if not text.isalpha() or not circumference.isalnum():
        return [False, "The input is not in the correct format!"]
    if not text.isupper():
        return [False, "Characters are not uppercase!"]
    return [True, "Correct input"]

def iscorrect_input_railfence(text, rail_num):
    if len(text) == 0:
        return [False, "The length is zero!"]
    if not text.isalpha() or not rail_num.isalnum():
        return [False, "The input is not in the correct format!"]
    if not text.isupper():
        return [False, "Characters are not uppercase!"]
    return [True, "Correct input"]
    