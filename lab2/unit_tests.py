import unittest
import utils
import sys
import encryptor
from solitaire import *
sys.path.append('../lab1/assign1')
from crypto import *

class Test(unittest.TestCase):  
    def test_generatePrivKey(self):
        (w, q, r) = generate_private_key();
        self.assertEquals(8, len(w))
        self.assertEquals(1, cryptoUtils.coprime(q, r))
        sortedW = w[:]
        sortedW.sort()
        self.assertEquals(sortedW, w)

    def test_generatePublicKey(self):
        (w, q, r) = generate_private_key();
        beta = create_public_key((w, q, r))
        self.assertEquals(8, len(beta))
    
    def test_encrypt_mh(self):
        (w, q, r) = generate_private_key();
        beta = create_public_key((w, q, r))
        test_string = "pelda"
        encrypted = encrypt_mh(test_string, beta)
        self.assertEquals(len(test_string), len(encrypted))

    def test_decrypt_mh(self):
        (w, q, r) = generate_private_key();
        beta = create_public_key((w, q, r))
        test_string = "pelda"
        encrypted = encrypt_mh(test_string, beta)
        decrypted = decrypt_mh(encrypted, (w,q,r))
        self.assertEquals(test_string, ''.join(decrypted))
    
    def test_solitaire_deck(self):
        deck = utils.getKeySolitaire()
        self.assertEquals(54, len(deck))
        sortedDeck = list(range(-2,53))
        sortedDeck.remove(0)
        deck.sort()
        self.assertEquals(sortedDeck, deck)

    def test_solitaire_encryptor(self):
        deck = utils.getKeySolitaire()
        plaintext = "pelda"
        text, offset = solitaireEncryptor(deck, plaintext, 0)
        self.assertEquals(len(plaintext), len(text))

    def test_solitaire_decrypted(self):
        deck = utils.getKeySolitaire()
        starterDeck = deck[:]
        plaintext = "pelda"
        text, offset = solitaireEncryptor(deck, plaintext, 0)
        decrypted_text, newOffset = solitaireEncryptor(starterDeck, text, 0)
        self.assertEquals(decrypted_text, plaintext)
        self.assertEquals(offset, newOffset)

    def test_solitaire_decrypted_offset(self):
        deck = utils.getKeySolitaire()
        starterDeck = deck[:]
        plaintext = "pelda"
        text, offset = solitaireEncryptor(deck, plaintext, 0)
        text, offset = solitaireEncryptor(deck, plaintext, offset)
        text, offset = solitaireEncryptor(deck, plaintext, offset)
        starterDeck = encryptor.decryptOffset(
            "solitaire", starterDeck, offset-len(plaintext))
        newOffset = offset
        decrypted_text, newOffset = encryptor.decrypt(
            "solitaire", starterDeck, text, 0)
        self.assertEquals(decrypted_text, plaintext)

if __name__ == '__main__':
    unittest.main()
