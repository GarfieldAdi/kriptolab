import utils
import encryptor
from io import StringIO
import socket
import json

host = 'localhost'
port = 10000
str_io_obj = StringIO()

def sendCipher(keyBlumBlum, text, generator, offsetBlumBlum, keySolitaire, offsetSol, s):
    if generator == "B":
        key = keyBlumBlum
        encryptedText, keyBlumBlum[0], newOffset = encryptor.encrypt("blumblum", keyBlumBlum, text, offsetBlumBlum)
        offset = offsetBlumBlum
        offsetBlumBlum = newOffset
    elif generator == "S":
        key = keySolitaire
        encryptedText, newOffset = encryptor.encrypt("solitaire", keySolitaire, text, offsetSol)
        offset = offsetSol
        offsetSol = newOffset
    else:
        print("Wrong input!")
        return
    s.sendall(json.dumps({
        "action": 0,
        "text": encryptedText, 
        "generator": generator,
        "offset": offset
    }).encode())

def decryptText(keyBlumBlum, text, generator, offset, offsetBlumBlum, keySolitaire, offsetSol, s):
    if generator == "B":
        if offset < offsetBlumBlum:
            keyBlumBlum = utils.getKeyBlumBlum()
            keyBlumBlum[0], offsetBlumBlum = encryptor.decryptOffset("blumblum", keyBlumBlum, offsetBlumBlum)
        elif offset > offsetBlumBlum:
            keyBlumBlum[0], offsetBlumBlum = encryptor.decryptOffset("blumblum", keyBlumBlum, offset-offsetBlumBlum)
        decryptedText, keyBlumBlum[0], newOffset = encryptor.decrypt("blumblum", keyBlumBlum, text, offsetBlumBlum)
        offsetBlumBlum = newOffset
        print("The decrypted text is: " + decryptedText)
                        
    elif generator == "S":
        if offset < offsetSol:
            keySolitaire = utils.getKeySolitaire()
            keySolitaire, offsetSol = encryptor.decryptOffset("solitaire", keySolitaire, offsetSol)
        elif offset > offsetBlumBlum:
            keySolitaire, offsetSol = encryptor.decryptOffset("solitaire", keySolitaire, offset-offsetSol)
                    
        decryptedText, newOffset = encryptor.decrypt("solitaire", keySolitaire, text, offsetSol)
        offsetSol = newOffset
        print("The decrypted text is: " + decryptedText)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host, port))
    received = s.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    if (data["cmd"] == "init"):
        print("Connected to the server")
        jsonSend = {"status": "OK"}
        dataOK = json.dumps(jsonSend)
        s.sendall(bytes(dataOK, encoding="utf-8"))
        end = False

        keySolitaire = utils.getKeySolitaire()
        offsetSol = 0
        keyBlumBlum = utils.getKeyBlumBlum()
        offsetBlumBlum = 0

        while not end:
            action = input("What do you want to do? (C)ipher, (D)ecipher, (E)nd?  ")
            if action == 'C':
                text = input("Text to cipher and send: ")
                generator = input("Generator to use: (B)lumBlum, (S)olitaire  ")
                sendCipher(keyBlumBlum, text, generator, offsetBlumBlum, keySolitaire, offsetSol, s)
            elif action == 'D':
                received = s.recv(1024)
                received = received.decode("utf-8")
                data = json.loads(received)
                text = data["text"]
                generator = data["generator"]
                offset = data["offset"]
                print("Got the text: " + text + " to decipher with: " + generator)
                decryptText(keyBlumBlum, text, generator, offset, offsetBlumBlum, keySolitaire, offsetSol, s)
            elif action == 'E':
                end = True
                s.sendall(json.dumps({
                    "action": 1
                }).encode())

    print("Connection ended")