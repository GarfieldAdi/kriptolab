#!/usr/bin/python3
import socket
import json
import numpy as np
import time
from _thread import *

PORT = 10000
SIZE = 1024

def clientthread(connections, ind):
    if ind == 0:
        otherInd = 1
    elif ind == 1:
        otherInd = 0
    while True:
        message = json.loads(connections[ind].recv(SIZE).decode())
        if message["action"] == 0:
            connections[otherInd].send(json.dumps({
                "text": message["text"], 
                "generator": message["generator"],
                "offset": message["offset"]
            }).encode())
        if message["action"] == 1:
            break

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = ('localhost', PORT)
    print('starting up on %s port %s' % server_address)
    sock.bind(server_address)

    sock.listen(1)

    people_connected = 0
    clients = [None, None]

    while people_connected < 2:
        print('waiting for a connection')
        connection, client_address = sock.accept()
        clients[people_connected] = connection
        print('connection from', client_address)

        connection.sendall(json.dumps({
            "cmd": "init",
        }).encode())

        data = json.loads(connection.recv(SIZE).decode())
        if "status" not in data or data["status"] != "OK":
            print("Error connecting")
            continue

        people_connected += 1
        start_new_thread(clientthread, (clients, 0))
        start_new_thread(clientthread, (clients, 1))

    while 1:
        pass

    sock.close()


if __name__ == "__main__":
    main()
