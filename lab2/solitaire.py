import random

deck = list(range(-2,53))
deck.remove(0)
random.shuffle(deck)

def firstStep(deck):
    index = deck.index(-2)
    if index == 0:
        deck = deck[1:]
        deck.append(-2)
        deck[52], deck[53] = deck[53], deck[52] 
    else:
        deck[index], deck[index-1] = deck[index-1], deck[index]

def secondStep(deck):
    index = deck.index(-1)
    if index == 0:
        deck = deck[1:]
        deck.append(-1)
        deck[52], deck[53] = deck[53], deck[52]
        deck[51], deck[52] = deck[52], deck[51]
    elif index == 1:
        newDeck = [deck[0]]
        newDeck.extend(deck[2:])
        newDeck.append(-1)
        deck.clear()
        deck.extend(newDeck)
    else:
        deck[index], deck[index-2] = deck[index-2], deck[index]

def thirdStep(deck):
    fstJoker = -2
    sndJoker = -1
    fstIndex = deck.index(-2)
    sndIndex = deck.index(-1)
    if (fstIndex > sndIndex):
        fstIndex, sndIndex = sndIndex, fstIndex
        fstJoker, sndJoker = sndJoker, fstJoker
    if (sndIndex < 53):
        newDeck = deck[sndIndex+1:54]
    else:
        newDeck = []
    newDeck.append(fstJoker)
    if (sndIndex - fstIndex > 1):
        newDeck.extend(deck[fstIndex+1:sndIndex])
    newDeck.append(sndJoker)
    if (fstIndex != 0):
        newDeck.extend(deck[0:fstIndex])
    deck.clear()
    deck.extend(newDeck)

def fourthStep(deck):
    value = deck[0]
    if value >= 0:
        newDeck = []
        newDeck.append(deck[0])
        newDeck.extend(deck[53-value:54])
        newDeck.extend(deck[1:53-value])
        deck.clear()
        deck.extend(newDeck)

def fifthStep(deck):
    value = deck[53]
    if (value < 0):
        return getKey(deck)
    else:
        key = deck[53 - value]
        if (key < 0):
            return 53
        else:
            return key

def getKey(deck):
    firstStep(deck)
    secondStep(deck)
    thirdStep(deck)
    fourthStep(deck)
    return fifthStep(deck)

def getOneTimePad(deck, length):
    length = length * 8
    binary = []
    for _ in range (length):
        key = getKey(deck)
        binary.append(key%2)
    return binary

def solitaireEncryptor(deck, plaintext, offset):
    text = [ord(char) for char in plaintext]
    oneTimePad = getOneTimePad(deck, len(text))
    cipherArray = []
    for i in range (len(text)):
        byte = int("".join(str(i) for i in oneTimePad[i*8:i*8+8]), 2)
        cipherArray.append(text[i] ^ byte)
    encryptedText = ''.join(chr(cipher) for cipher in cipherArray)
    return encryptedText, offset+len(encryptedText)

def solitaireEncryptorOffset(deck, offset):
    oneTimePad = getOneTimePad(deck, offset)
    return deck