import blumblumshub as bbs
import solitaire as s
import random
import utils

def encrypt(generator, key, text, offset):
    if generator == 'solitaire':
        return s.solitaireEncryptor(key, text, offset)
    elif generator == 'blumblum':
        return bbs.blumEncryptor(key[0], key[1], text, offset)

def encryptOffset(generator, key, offset):
    if generator == 'solitaire':
        return s.solitaireEncryptorOffset(key, offset)
    elif generator == 'blumblum':
        return bbs.blumEncryptorOffset(key[0], key[1], offset)

def decrypt(generator, key, text, offset):
    if generator == 'solitaire':
        return s.solitaireEncryptor(key, text, offset)
    elif generator == 'blumblum':
        return bbs.blumEncryptor(key[0], key[1], text, offset)

def decryptOffset(generator, key, offset):
    if generator == 'solitaire':
        return s.solitaireEncryptorOffset(key, offset)
    elif generator == 'blumblum':
        return bbs.blumEncryptorOffset(key[0], key[1], offset)

def main():
    keyBlumBlum = utils.getKeyBlumBlum();
    keySolitaire = utils.getKeySolitaire();
    text = 'pelda hazira'
    encryptedText, offset = encrypt('solitaire', keySolitaire[:], text, 0)
    encryptedText, offset = decrypt('solitaire', keySolitaire, encryptedText, 0)
    print(encryptedText)
    encryptedText, lastSeed, offset = encrypt('blumblum', keyBlumBlum[:], text, 0)
    encryptedText, lastSeed, offset = decrypt('blumblum', keyBlumBlum, encryptedText, 0)
    print(encryptedText)

if __name__ == "__main__":
    main()

