import random

def blumblumshab(length, seed, n):
    length = length*8
    x = []
    x.append((seed * seed) % n)
    z = []
    z.append(x[0] % 2)
    for i in range(1, length):
        xValue = (x[i-1] * x[i-1]) % n
        x.append(xValue)
        z.append(xValue % 2)
    return ''.join(map(str,z)), x[length-1]

def blumEncryptor(seed, n, plaintext, offset):
    text = [ord(char) for char in plaintext]
    oneTimePad, lastSeed = blumblumshab(len(text), seed, n)
    cipherArray = []
    for i in range (len(text)):
        byte = int("".join(str(i) for i in oneTimePad[i*8:i*8+8]), 2)
        cipherArray.append(text[i] ^ byte)
    encryptedText = ''.join(chr(cipher) for cipher in cipherArray)
    return encryptedText, lastSeed, offset+len(encryptedText)

def blumEncryptorOffset(seed, n, offset):
    oneTimePad, lastSeed = blumblumshab(len(text), seed, n)
    return lastSeed, n
