import utils
import encryptor
from io import StringIO
import socket
import json
from _thread import *
import random
import utils
from solitaire import *
import sys
import time
sys.path.append('../lab1/assign1')
from crypto import *

host = 'localhost'
port = 10000
str_io_obj = StringIO()
myport = random.randint(40000, 65535)


def sendCipher(text, keySolitaire, offsetSol, s):
    key = keySolitaire
    encryptedText, newOffset = encryptor.encrypt(
        "solitaire", keySolitaire, text, offsetSol)
    offset = offsetSol
    offsetSol = newOffset
    s.sendall(json.dumps({
        "text": encryptedText,
        "offset": offset
    }).encode())


def decryptText(text, offset, keySolitaire, offsetSol, s, starterKey):
    if offset < offsetSol:
        keySolitaire = starterKey
        keySolitaire, offsetSol = encryptor.decryptOffset(
            "solitaire", keySolitaire, offsetSol)
    elif offset > offsetSol:
        keySolitaire, offsetSol = encryptor.decryptOffset(
            "solitaire", keySolitaire, offset-offsetSol)

    decryptedText, newOffset = encryptor.decrypt(
        "solitaire", keySolitaire, text, offsetSol)
    offsetSol = newOffset
    print("The decrypted text is: " + decryptedText)


def changeKey(s, private_key, public_key):
    s.sendall(json.dumps({
        "action": 'K',
    }).encode())
    received = s.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    private_key = data["private"]
    public_key = data["public"]


def init(s, data, private_key, public_key):
    print("Connected to the server")
    private_key = data["private"]
    public_key = data["public"]
    jsonSend = {"status": "OK"}
    dataOK = json.dumps(jsonSend)
    s.sendall(bytes(dataOK, encoding="utf-8"))
    return private_key, public_key


def list_users(s):
    s.sendall(json.dumps({
        "action": 'U',
    }).encode())
    received = s.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    print("The available users are: ")
    list_users = [u for u in data["users"] if u != myport]
    print(list_users)

def getCommonDeckMessager(s, public_key, private_key, user_public_key):
    s.sendall(json.dumps({
        "user_key": public_key,
    }).encode())
    received = s.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    deck = data["half_deck"]
    deck = [int(''.join(decrypt_mh(x, private_key))) for x in deck]
    secondDeck = utils.getKeySolitaire()
    secondDeck = [x for x in secondDeck if x not in deck]
    random.shuffle(secondDeck)
    encryptedDeck = [encrypt_mh(str(x), user_public_key) for x in secondDeck]
    s.sendall(json.dumps({
        "half_deck": encryptedDeck
    }).encode())
    commonDeck = deck[:]
    commonDeck.extend(secondDeck)
    print(commonDeck)
    return commonDeck

def getCommonDeckWait(connection, private_key):
    received = connection.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    public_key_user = data["user_key"]
    deck = utils.getKeySolitaire()
    random.shuffle(deck)
    encryptedDeck = [encrypt_mh(str(x), public_key_user) for x in deck]
    connection.sendall(json.dumps({
        "half_deck": encryptedDeck[0:27]
    }).encode())
    received = connection.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    commonDeck = deck[0:27];
    half_deck = data["half_deck"]
    deck = [int(''.join(decrypt_mh(x, private_key))) for x in half_deck]
    commonDeck.extend(deck)
    print(commonDeck)
    return [commonDeck, public_key_user]

def sendMessages(s, keySolitaire):
    offsetSol = 0
    starterDeck = keySolitaire[:]
    while 1:
        action = input("What do you want to do? (C)ipher, (D)ecipher, (E)nd?  ")
        if action == 'C':
            text = input("Text to cipher and send: ")
            sendCipher(text, keySolitaire, offsetSol, s)
        elif action == 'D':
            received = s.recv(1024)
            received = received.decode("utf-8")
            data = json.loads(received)
            text = data["text"]
            offset = data["offset"]
            print("Got the text: " + text)
            decryptText(text, offset, keySolitaire, offsetSol, s, starterDeck)
        elif action == 'E':
            break

def message(s, public_key, private_key):
    user = input("With which user do you want to talk?")
    s.sendall(json.dumps({
        "action": 'M',
        "user": user
    }).encode())
    received = s.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    public_key_user = data["user_key"]
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as w:
        w.bind((host, myport+1))
        w.connect((host, int(user)+1))
        commonDeck = getCommonDeckMessager(w, public_key, private_key, public_key_user)
        sendMessages(w, commonDeck)

def wait(s, private_key):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as w:
        w.bind((host, myport+1))
        w.listen(1)
        connection, client_address = w.accept()
        [commonDeck, public_key_user] = getCommonDeckWait(connection, private_key)
        sendMessages(connection, commonDeck)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((host, myport))
    s.connect((host, port))
    received = s.recv(1024)
    received = received.decode("utf-8")
    data = json.loads(received)
    private_key = None
    public_key = None
    if (data["cmd"] == "init"):
        private_key, public_key = init(s, data, private_key, public_key)
    end = False
    while not end:
        action = input(
            "What do you want to do? New key(K), list users(U), send message(M), wait for user (W), log out(L)?")
        if action == 'K':
            changeKey(s, private_key, public_key)
        elif action == 'U':
            list_users(s)
        elif action == 'L':
            s.sendall(json.dumps({
                "action": 'L',
            }).encode())
            end = True
        elif action == 'M':
            message(s, public_key, private_key)
        elif action == 'W':
            wait(s, private_key)
        else:
            print('Wrong input!!!') 

    print("Connection ended")
