#!/usr/bin/python3
import socket
import json
import numpy as np
import time
from _thread import *
import sys
sys.path.append('../lab1/assign1')
from crypto import *

PORT = 10000
SIZE = 1024


def clientthread(connections, address, public_keys):
    connection = connections[address]
    while True:
        message = json.loads(connection.recv(SIZE).decode())
        if message["action"] == 'K':
            private_key = generate_private_key()
            public_key = create_public_key(private_key)
            public_keys[address] = public_key
            connection.send(json.dumps({
                "public": public_key,
                "private": private_key
            }).encode())
        elif message["action"] == 'U':
            users = list(public_keys.keys())
            connection.send(json.dumps({
                "users": users
            }).encode())
        elif message["action"] == 'L':
            connection.close();
            connections.pop(address, None)
            public_keys.pop(address, None)
            break
        elif message["action"] == 'M':
            user = int(message["user"])
            print(public_keys[user])
            connection.send(json.dumps({
                "user_key": public_keys[user]
            }).encode())


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = ('localhost', PORT)
    print('starting up on %s port %s' % server_address)
    sock.bind(server_address)

    sock.listen(1)

    clients = {}
    public_keys = {}

    while 1:
        print(clients)
        print('waiting for a connection')
        connection, client_address = sock.accept()
        clients[client_address[1]] = connection
        print('connection from', client_address)

        private_key = generate_private_key()
        public_key = create_public_key(private_key)
        public_keys[client_address[1]] = public_key

        connection.send(json.dumps({
            "cmd": "init",
            "public": public_key,
            "private": private_key
        }).encode())

        data = json.loads(connection.recv(SIZE).decode())
        if "status" not in data or data["status"] != "OK":
            print("Error connecting")
            continue
        start_new_thread(
            clientthread, (clients, client_address[1], public_keys))

    while 1:
        pass

    sock.close()


if __name__ == "__main__":
    main()
